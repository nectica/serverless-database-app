import './App.css';
import {EasybaseProvider, useEasybase} from 'easybase-react';
import React, { useEffect } from 'react';
import ebconfig from './ebconfig';
import NewNoteButton from './NewNoteButton';

function App() {
  return (
    <div className="App" style={{ display: "flex", justifyContent: "center" }}>
        <EasybaseProvider ebconfig={ebconfig} >
            <Notes />
        </EasybaseProvider>
    </div>
  );
}

function Notes() {
    const { Frame, sync, configureFrame } = useEasybase();
  

  const noteRootStyle = {
    border: "2px #0af solid",
    borderRadius: 9,
    margin: 20,
    backgroundColor: "#efefef",
    padding: 6
  };
   useEffect(() => {
    configureFrame({tableName: "NOTES", limit:10});
    sync();
  }, []);
  console.log('render', Frame() );
  return (
    <div style={{ width: 400 }}>
      {Frame().map(ele => 
        <div style={noteRootStyle}>
          <h3>{ele.title}</h3>
          <p>{ele.description}</p>
          <small>{ele.createdat}</small>
        </div>
      )}
      <NewNoteButton />
    </div>
  )
}

export default App;