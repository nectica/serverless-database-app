import { useEasybase } from "easybase-react";

 function NewNoteButton() {
    const {Frame, sync} = useEasybase();
    const buttonStyle = {
        position: "absolute",
        left: 10,
        top: 10,
        fontSize:21,
    }
    const handleClick = () => {
        const newTitle = prompt("Escribe titulo");
        const newDescription = prompt("Ingres descripcion");
        Frame().push({
            title: newTitle,
            description: newDescription,
            createdAt: new Date().toISOString()
        });
        sync();
    }

    return <button style={buttonStyle} onClick={handleClick} >📓 Add note 📓</button>
    
}
export default NewNoteButton ;
